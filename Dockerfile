FROM webdevops/php-nginx:7.3

RUN apt-get install apt-transport-https

# add mssql tools package
RUN echo "deb https://packages.sury.org/php/ buster main" > /etc/apt/sources.list.d/php.list
RUN wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg
RUN wget -qO - https://packages.microsoft.com/keys/microsoft.asc | apt-key add -
RUN curl https://packages.microsoft.com/config/debian/10/prod.list > /etc/apt/sources.list.d/mssql-release.list
RUN apt-get update

# install mssql tools
RUN ACCEPT_EULA=Y apt-get install msodbcsql17 mssql-tools -y 
RUN ACCEPT_EULA=Y apt-get install unixodbc-dev libgssapi-krb5-2 -y

# install sqlsrv dependencies
RUN pecl install sqlsrv pdo_sqlsrv \
    && docker-php-ext-configure intl \
    && docker-php-ext-install pdo pdo_mysql intl \
    && docker-php-ext-enable redis

# add depdendencies to php.ini file
RUN printf "; priority=20\nextension=sqlsrv.so\n" > /usr/local/etc/php/conf.d/sqlsrv.ini
RUN printf "; priority=30\nextension=pdo_sqlsrv.so\n" > /usr/local/etc/php/conf.d/pdo_sqlsrv.ini